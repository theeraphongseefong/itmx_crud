package constant

const (
	MessageSuccess    string = "success"
	MessageFailure    string = "failure"
	ErrRecordNotFound string = "record not found"
	ErrNotUpdate      string = "not update"
	ErrDatabase       string = "database error"
)

package config

import (
	"log"
	"reflect"
	"strings"

	"github.com/spf13/viper"
)

type AppConfig struct {
	Port string
}

func LoadConfig() *AppConfig {
	// Set up Viper to read environment variables from a .env file
	viper.SetConfigFile(".env")

	// convert _ to dot in env variable
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("fatal error config file: %+v", err)
	}

	var cfg AppConfig
	bindEnvs(cfg)
	if err := viper.Unmarshal(&cfg); err != nil {
		log.Fatalf("unable to decode config into struct, %+v", err)
	}

	return &cfg
}

// bindEnvs function will bind file to struct model
func bindEnvs(iFace interface{}, parts ...string) {
	ifv := reflect.ValueOf(iFace)
	ift := reflect.TypeOf(iFace)
	for i := 0; i < ift.NumField(); i++ {
		v := ifv.Field(i)
		t := ift.Field(i)
		tv, ok := t.Tag.Lookup("env")
		if !ok {
			continue
		}
		switch v.Kind() {
		case reflect.Struct:
			bindEnvs(v.Interface(), append(parts, tv)...)
		default:
			viper.BindEnv(strings.Join(append(parts, tv), "."))
		}
	}
}

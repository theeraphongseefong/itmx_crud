# itmx_crud

itmx_crud-api build with Golang 

## Table of Contents
- [Framework](#framework)
- [API Listing](#api-listing)
- [Build With](#build-with)
- [How To Run](#how-to-run)

## Build With
* Echo - Go web framework
* Clean - Architecture

## Framework
- [Golang](https://go.dev)
- [Echo](https://echo.labstack.com)
- [Gorm](https://gorm.io)

## API Listing
| API | Method | Endpoint |
|-----|-----|-----|
| create     | POST | /customers/create |
| find by id | GET  | /customers/{id} |
| update     | PUT  | /customers/update |
| delete     | DELETE | /customers/{id} |



## How To Run

- 1. run this command in terminal
```
go run main.go
```
- 2. go to 
[http://localhost:8080/api/v1/swagger/](http://localhost:8080/api/v1/swagger/)

## Authors
- [theeraphongseefong@gmail.com](https://gitlab.com/theeraphongseefong/)

package repository

import (
	"gorm.io/gorm"
)

type Repo struct {
	Customer RepoCustomer
}

func NewRepository(db *gorm.DB) *Repo {
	return &Repo{
		Customer: NewCustomerRepo(db),
	}
}

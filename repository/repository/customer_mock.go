package repository

import (
	"errors"
	"itmx_crud/constant"
	"itmx_crud/model"

	"github.com/stretchr/testify/mock"
)

type CustomerRepositoryMock struct {
	mock.Mock
}

func NewCustomerRepoMock() *CustomerRepositoryMock {
	return &CustomerRepositoryMock{}
}

func (m *CustomerRepositoryMock) CreateCustomer(req *model.Customer) error {
	if req.Name == "" || req.Age == 0 {
		return errors.New(constant.ErrDatabase)
	}
	return nil
}

func (m *CustomerRepositoryMock) FindCustomerByID(id uint64) (*model.Customer, error) {

	if id <= 0 {
		return nil, errors.New(constant.ErrDatabase)
	}

	// ทำการบันทึกการเรียกใช้งานฟังก์ชันใน mock
	args := m.Called(id)

	// ดึงค่าที่ต้องการจากการเรียกใช้งานฟังก์ชันใน mock
	res := args.Get(0).(model.Customer)
	err := args.Error(1)

	if id != res.ID {
		return nil, errors.New(constant.ErrDatabase)
	}

	return &res, err
}

func (m *CustomerRepositoryMock) UpdateCustomer(model *model.Customer) error {

	if model.ID <= 0 {
		return errors.New(constant.ErrDatabase)
	}

	if model.Name == "" || model.Age == 0 {
		return errors.New(constant.ErrDatabase)
	}

	return nil
}

func (m *CustomerRepositoryMock) DeleteCustomer(id uint) error {

	if id <= 0 {
		return errors.New(constant.ErrDatabase)
	}

	return nil
}

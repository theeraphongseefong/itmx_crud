package repository

import (
	"errors"
	"itmx_crud/constant"
	"itmx_crud/model"

	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func NewCustomerRepo(db *gorm.DB) RepoCustomer {
	return &repository{
		db: db,
	}
}

type RepoCustomer interface {
	CreateCustomer(model.Customer) error
	FindCustomerByID(id uint64) (*model.Customer, error)
	UpdateCustomer(model.Customer) error
	DeleteCustomer(id uint64) error
}

type Customer struct {
	ID   uint64 `gorm:"primaryKey"`
	Name string
	Age  uint
}

func (db repository) CreateCustomer(customer model.Customer) error {
	err := db.db.Save(&customer).Error
	if err != nil {
		return err
	}
	return nil
}

func (db repository) FindCustomerByID(id uint64) (*model.Customer, error) {
	customer := Customer{}
	err := db.db.Take(&customer, id).Error
	if err != nil {
		return nil, err
	}

	resp := model.Customer{
		ID:   customer.ID,
		Name: customer.Name,
		Age:  customer.Age,
	}
	return &resp, nil
}

func (db repository) UpdateCustomer(req model.Customer) error {

	customerEntity := Customer{}

	tx := db.db.Model(&customerEntity).Where("id = ?", req.ID).Updates(Customer{Name: req.Name, Age: req.Age})
	if tx.Error != nil {
		return tx.Error
	}

	if tx.RowsAffected == 0 {
		return errors.New(constant.ErrNotUpdate)
	}
	return nil
}

func (db repository) DeleteCustomer(id uint64) error {
	customerEntity := Customer{}

	err := db.db.Delete(&customerEntity, id).Error
	if err != nil {
		return err
	}
	return nil
}

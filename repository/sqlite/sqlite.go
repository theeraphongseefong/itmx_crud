package sqlite

import (
	"itmx_crud/repository/repository"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func ConnectDatabase() (*gorm.DB, error) {

	// Open a connection to the SQLite
	connectDB, err := gorm.Open(sqlite.Open("./repository/sqlite/customer.sqlite"), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// AutoMigrate will create the 'customer' table based on the User struct
	err = connectDB.AutoMigrate(&repository.Customer{})
	if err != nil {
		return nil, err
	}

	// Query the database to verify the records were created
	customers := []repository.Customer{}
	connectDB.Find(&customers)
	if len(customers) == 0 {
		// Create two customer records
		customer1 := repository.Customer{Name: "John", Age: 30}
		customer2 := repository.Customer{Name: "Alice", Age: 25}
		// Save the records to the database
		connectDB.Create(&customer1)
		connectDB.Create(&customer2)
	}

	return connectDB, nil
}

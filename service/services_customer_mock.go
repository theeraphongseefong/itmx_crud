package services

import (
	"errors"
	"itmx_crud/constant"
	"itmx_crud/model"

	"github.com/stretchr/testify/mock"
)

type customerServiceMock struct {
	mock.Mock
}

func NewCustomerServiceMock() *customerServiceMock {
	return &customerServiceMock{}
}

func (m *customerServiceMock) CreateCustomer(req *model.CreateCustomerRequest) error {

	if req.Name == "" || req.Age == 0 {
		return errors.New(constant.ErrRecordNotFound)
	}

	return nil
}

func (m *customerServiceMock) FindCustomerByID(id uint64) (*model.CustomerResponse, error) {
	if id <= 0 {
		return nil, errors.New(constant.ErrRecordNotFound)
	}

	// ทำการบันทึกการเรียกใช้งานฟังก์ชันใน mock
	args := m.Called(id)

	// ดึงค่าที่ต้องการจากการเรียกใช้งานฟังก์ชันใน mock
	res := args.Get(0).(model.CustomerResponse)
	err := args.Error(1)

	if id != res.ID {
		return nil, errors.New(constant.ErrRecordNotFound)
	}

	return &res, err
}

func (m *customerServiceMock) UpdateCustomer(model *model.Customer) error {

	if model.ID <= 0 {
		return errors.New(constant.ErrRecordNotFound)
	}

	if model.Name == "" || model.Age == 0 {
		return errors.New(constant.ErrRecordNotFound)
	}

	return nil
}

func (m *customerServiceMock) DeleteCustomer(id uint) error {

	if id <= 0 {
		return errors.New(constant.ErrRecordNotFound)
	}

	return nil
}

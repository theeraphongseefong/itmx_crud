package services

import (
	"itmx_crud/model"
	"itmx_crud/repository/repository"
)

type Services interface {
	CreateCustomer(model.CreateCustomerRequest) error
	FindCustomerByID(id uint64) (*model.CustomerResponse, error)
	DeleteCustomer(id uint64) error
	UpdateCustomer(model.UpdateCustomerRequest) error
}

type services struct {
	repo repository.RepoCustomer
}

func NewService(repo repository.Repo) *services {
	return &services{
		repo: repo.Customer,
	}
}

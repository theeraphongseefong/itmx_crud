package services

import (
	"itmx_crud/model"
)

// CreateCustomer implements Services.
func (s *services) CreateCustomer(request model.CreateCustomerRequest) error {
	customer := model.Customer{
		Name: request.Name,
		Age:  request.Age,
	}
	err := s.repo.CreateCustomer(customer)
	if err != nil {
		return err
	}
	return nil
}

// DeleteCustomer implements Services.
func (s *services) DeleteCustomer(id uint64) error {
	err := s.repo.DeleteCustomer(id)
	if err != nil {
		return err
	}

	return nil
}

// UpdateCustomer implements Services.
func (s *services) UpdateCustomer(request model.UpdateCustomerRequest) error {
	customer := model.Customer{
		ID:   request.ID,
		Name: request.Name,
		Age:  request.Age,
	}
	err := s.repo.UpdateCustomer(customer)
	if err != nil {
		return err
	}
	return nil
}

// FindCustomerByID implements Services.
func (s *services) FindCustomerByID(id uint64) (*model.CustomerResponse, error) {
	customer, err := s.repo.FindCustomerByID(id)
	if err != nil {
		return nil, err
	}

	resp := &model.CustomerResponse{
		ID:   customer.ID,
		Name: customer.Name,
		Age:  customer.Age,
	}

	return resp, nil
}

package services

import (
	"itmx_crud/model"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
)

func TestNewCustomerServiceMock(t *testing.T) {
	tests := []struct {
		name string
		want *customerServiceMock
	}{
		{
			name: "",
			want: &customerServiceMock{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCustomerServiceMock(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCustomerServiceMock() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_customerServiceMock_CreateCustomer(t *testing.T) {
	type fields struct {
		Mock mock.Mock
	}
	type args struct {
		req *model.CreateCustomerRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test_pass",
			args: args{
				req: &model.CreateCustomerRequest{
					Name: "foo",
					Age:  1,
				},
			},
			wantErr: false,
		},
		{
			name: "test_fail_name_is_nil",
			args: args{
				req: &model.CreateCustomerRequest{
					Name: "",
					Age:  12,
				},
			},
			wantErr: true,
		},
		{
			name: "test_fail_age_is_zero",
			args: args{
				req: &model.CreateCustomerRequest{
					Name: "foo",
					Age:  0,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &customerServiceMock{
				Mock: tt.fields.Mock,
			}
			if err := m.CreateCustomer(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("customerServiceMock.CreateCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_customerServiceMock_FindCustomerByID(t *testing.T) {
	type fields struct {
		Mock mock.Mock
	}
	type args struct {
		id uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.CustomerResponse
		wantErr bool
	}{
		{
			name: "test_pass",
			args: args{
				id: 0,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			mock := customerServiceMock{}
			mock.On("FindCustomerByID", tt.args.id).Return(tt.want, nil)

			got, err := mock.FindCustomerByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("customerServiceMock.FindCustomerByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("customerServiceMock.FindCustomerByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_customerServiceMock_UpdateCustomer(t *testing.T) {
	type fields struct {
		Mock mock.Mock
	}
	type args struct {
		model *model.Customer
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test_pass",
			args: args{
				model: &model.Customer{
					ID:   1,
					Name: "foo",
					Age:  12,
				},
			},
			wantErr: false,
		},
		{
			name: "test_fail_id_is_zero",
			args: args{
				model: &model.Customer{
					ID:   0,
					Name: "foo",
					Age:  12,
				},
			},
			wantErr: true,
		},
		{
			name: "test_fail_name_is_nil",
			args: args{
				model: &model.Customer{
					ID:   1,
					Name: "",
					Age:  12,
				},
			},
			wantErr: true,
		},
		{
			name: "test_fail_age_is_zero",
			args: args{
				model: &model.Customer{
					ID:   1,
					Name: "",
					Age:  0,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &customerServiceMock{
				Mock: tt.fields.Mock,
			}
			if err := m.UpdateCustomer(tt.args.model); (err != nil) != tt.wantErr {
				t.Errorf("customerServiceMock.UpdateCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_customerServiceMock_DeleteCustomer(t *testing.T) {
	type fields struct {
		Mock mock.Mock
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test_pass",
			args: args{
				id: 1,
			},
			wantErr: false,
		},
		{
			name: "test_fail",
			args: args{
				id: 0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &customerServiceMock{
				Mock: tt.fields.Mock,
			}
			if err := m.DeleteCustomer(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("customerServiceMock.DeleteCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

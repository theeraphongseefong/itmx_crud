package utility

import (
	"itmx_crud/constant"
	"itmx_crud/model"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

func ResponseMessage(c echo.Context, data interface{}, err error) error {
	res := model.ResponseModel{}

	if err != nil {
		res.Message = constant.MessageFailure

		if _, ok := err.(validator.ValidationErrors); ok {
			res.Data = err.Error()
			return c.JSON(http.StatusBadRequest, res)
		}

		res.Data = err.Error()
		return c.JSON(http.StatusNotFound, res)
	} else {
		res.Message = constant.MessageSuccess
		res.Data = data
		return c.JSON(http.StatusOK, res)
	}
}

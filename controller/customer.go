package controller

import (
	"itmx_crud/model"
	srv "itmx_crud/service"
	"itmx_crud/utility"
	"log"
	"strconv"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type CustomerController interface {
	Create(echo.Context) error
	FindByID(echo.Context) error
	Update(echo.Context) error
	Delete(echo.Context) error
}

type customerController struct {
	customer srv.Services
}

var validate = validator.New()

// Create godoc
// @Summary Create
// @Description /customers/create
// @Accept json
// @Tags Customers
// @Produce json
// @Consumes application/json
// @Param Request body model.CreateCustomerRequest true "Request body"
// @Success 200 {object} model.ResponseModel{}
// @Failure 400 {object} model.ResponseModel{}
// @Failure 404 {object} model.ResponseModel{}
// @Failure 500 {object} model.ResponseModel{}
// @Router /customers/create [post]
// Create implements CustomerController.
func (ctrl *customerController) Create(c echo.Context) error {
	req := model.CreateCustomerRequest{}

	//State: bind http body and query params
	if err := c.Bind(&req); err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	//State: validate http body
	if err := validate.Struct(req); err != nil {
		log.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	err := ctrl.customer.CreateCustomer(req)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}

	return utility.ResponseMessage(c, nil, err)
}

// Delete godoc
// @Summary Delete
// @Description /customers/{id}
// @Accept json
// @Tags Customers
// @Produce json
// @Consumes application/json
// @Param        id   path      int  true  "ID"
// @Success 200 {object} model.ResponseModel{}
// @Failure 400 {object} model.ResponseModel{}
// @Failure 404 {object} model.ResponseModel{}
// @Failure 500 {object} model.ResponseModel{}
// @Router /customers/{id} [delete]
// Delete implements CustomerController.
func (ctrl *customerController) Delete(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	err = ctrl.customer.DeleteCustomer(id)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	return utility.ResponseMessage(c, nil, err)
}

// FindByID godoc
// @Summary FindByID
// @Description /customers/{id}
// @Accept json
// @Tags Customers
// @Produce json
// @Consumes application/json
// @Param        id   path      int  true  "ID"
// @Success 200 {object} model.ResponseModel{}
// @Failure 400 {object} model.ResponseModel{}
// @Failure 404 {object} model.ResponseModel{}
// @Failure 500 {object} model.ResponseModel{}
// @Router /customers/{id} [get]
// FindByID implements CustomerController.
func (ctrl *customerController) FindByID(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	resp, err := ctrl.customer.FindCustomerByID(id)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	return utility.ResponseMessage(c, resp, err)
}

// Update godoc
// @Summary Update
// @Description /customers/update
// @Accept json
// @Tags Customers
// @Produce json
// @Consumes application/json
// @Param Request body model.UpdateCustomerRequest true "Request body"
// @Success 200 {object} model.ResponseModel{}
// @Failure 400 {object} model.ResponseModel{}
// @Failure 404 {object} model.ResponseModel{}
// @Failure 500 {object} model.ResponseModel{}
// @Router /customers/update [put]
// Update implements CustomerController.
func (ctrl *customerController) Update(c echo.Context) error {
	req := model.UpdateCustomerRequest{}

	//State: bind http body and query params
	if err := c.Bind(&req); err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}
	//State: validate http body
	if err := validate.Struct(req); err != nil {
		log.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	err := ctrl.customer.UpdateCustomer(req)
	if err != nil {
		log.Println("err", err)
		return utility.ResponseMessage(c, nil, err)
	}

	return utility.ResponseMessage(c, nil, err)
}

func NewCustomerController(
	customer srv.Services,
) CustomerController {
	return &customerController{
		customer: customer,
	}
}

package model

type ResponseModel struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type Customer struct {
	ID   uint64
	Name string
	Age  uint
}

type CustomerResponse struct {
	ID   uint64 `json:"id"`
	Name string `json:"name"`
	Age  uint   `json:"age"`
}

type CreateCustomerRequest struct {
	Name string `query:"name" json:"name" validate:"required"`
	Age  uint   `query:"age" json:"age" validate:"required"`
}

type UpdateCustomerRequest struct {
	ID   uint64 `json:"id" validate:"required"`
	Name string `json:"name" validate:"required"`
	Age  uint   `json:"age" validate:"required"`
}
